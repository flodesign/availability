<?php
/*
Plugin Name: Availability system
Plugin URI:
Description: Availability calendar system for Beachfront Villas
Version:     1.0
Author:      Craig Thompson
Author URI:  http://flodesign.co.uk
License:     GNU GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html
Text Domain: availability
*/

require_once( 'classes/calendar.php' );
require_once( 'classes/search.php' );

function check_for_villa_page() {
	if ( is_singular( 'villa' ) ) :
		new Calendar();
	endif;
}

function check_for_search( $query ) {
	if ( is_admin() || ! $query->is_main_query() ) {
		return;
	}

	if ( isset( $_POST['flo_search'] ) ):
		if ( isset( $_POST['arrival-date'] ) && isset( $_POST['departure-date'] ) ) {
			$startdate = DateTime::createFromFormat( 'd/m/Y', $_POST['arrival-date'] )->format( 'Ymd' );
			$enddate   = DateTime::createFromFormat( 'd/m/Y', $_POST['departure-date'] )->format( 'Ymd' );
		} else {
			return;
		}

		//Get bookings between start and end date

		$bookingargs = array(
			'post_type'      => 'booking',
			'posts_per_page' => - 1,
			'meta_query'     => array(
				'relation' => 'OR',
				array(
					'key'     => 'start_date',
					'value'   => array( $startdate, $enddate ),
					'compare' => 'BETWEEN',
					'type'    => 'DATE'
				),
				array(
					'key'     => 'end_date',
					'value'   => array( $startdate, $enddate ),
					'compare' => 'BETWEEN',
					'type'    => 'DATE'
				),
			)
		);

		$bookingQuery = new WP_Query( $bookingargs );

		$bookingIDs = array();

		//Get an array of the booked villas from the booking posts
		while ( $bookingQuery->have_posts() ): $bookingQuery->the_post();
			$postID = get_the_ID();
			$villas = get_field( 'villa', $postID );
			foreach ( $villas as $villa ) {
				$bookingIDs[] = $villa->ID;
			}
		endwhile;

		$query->set( 'post_type', 'villa' );
		$query->set( 'post__not_in', $bookingIDs );
	endif;
}

function flo_search_template( $template ) {
	if ( isset( $_POST['flo_search'] ) ):
		return locate_template( array( 'flo_search.php' ) );
	endif;

	return $template;
}

add_action( 'template_redirect', 'check_for_villa_page' );
//add_action( 'template_redirect', 'check_for_search' );
add_action( 'pre_get_posts', 'check_for_search', 1 );
add_filter( 'template_include', 'flo_search_template' );

function flo_register_query_vars( $vars ) {
	$vars[] = 'arrival-date';
	$vars[] = 'departure-date';
	$vars[] = 'flo_search';
	$vars[] = 'post_type';

	return $vars;
}

add_filter( 'query_vars', 'flo_register_query_vars' );