<?php


class Search {

	public function __construct() {
	}

	public function validate_dates( $startdate, $enddate ) {
		if ( $startdate == '' || $enddate == '' ):
			return false;
		endif;

		if ( ! $this->date_validator( $startdate ) || ! $this->date_validator( $enddate ) ):
			return false;
		else:
			return true;
		endif;
	}

	public function get_bookings( $start, $end ) {
		//Get all bookings that are between the start and end dates
		$booking_args = array(
			'post_type'  => 'bookings',
			'meta_query' => array(
				'relation' => 'AND',
				array(
					'key'     => 'start_date',
					'compare' => '>=',
					'value'   => $start
				),
				array(
					'key'     => 'end_date',
					'compare' => '<=',
					'value'   => $end
				)
			)
		);

		$bookings_query = new WP_Query( $booking_args );

		var_dump($bookings_query->get_posts());
		ob_flush();

		if ( $bookings_query->have_posts() ):
			$bookings = array();
			while ( $bookings_query->have_posts() ): the_post();
				$bookings[] = $post->ID;
			endwhile;
			$villa_args = array(
				'post_type'  => 'villa',
				'meta_query' => array(
					'key'     => 'bookings',
					'compare' => 'IN',
					'value'   => $bookings
				)
			);

			$villas = new WP_Query( $villa_args );

			return $villas;
		else:
			return array( $start, $end );
		endif;
	}

	private function date_validator( $date ) {
		$d = DateTime::createFromFormat( 'd-m-Y', $date );

		return $d && $d->format( 'd-m-Y' ) === $date;
	}
}