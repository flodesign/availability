<?php

class Calendar {

	/**
	 * Calendar constructor.
	 */
	public function __construct() {
		add_shortcode( 'flodesign_calendar', array( $this, 'handle_shortcode' ) );
		require_once( plugin_dir_path( dirname( __FILE__ ) ) . '../advanced-custom-fields-pro/acf.php' );
	}

	/***** Properties *****/

	private $dayLabels = [ 'M', 'T', 'W', 'T', 'F', 'S', 'S' ];

	private $currentYear = 0;

	private $currentMonth = 0;

	private $currentDate = null;

	private $daysInMonth = 0;

	private $currentDay = null;

	/***** Methods *****/

	public function handle_shortcode( $atts ) {
		return $this->get_calendar( $atts['id'] );
	}

	/**
	 * Generates a calendar
	 *
	 * Method to generate html to be placed in a template hook. Uses bookings from get_bookings() and filters based on
	 * calendar type (admin or user).
	 *
	 * @param int $post_id
	 *
	 * @return string $calendar
	 */
	public function get_calendar( $post_id ) {
		$bookings = $this->get_bookings( $post_id );
		$calendar = $this->generate_calendar( $bookings );

		return $calendar;
	}

	/**
	 * Gets the bookings for a given villa
	 *
	 * Helper method to return an array of Booking objects, which can then be displayed on an availability calendar.
	 *
	 * @param int $post_id
	 *
	 * @return array $bookings
	 */
	private function get_bookings( $post_id ) {
		$args = array(
			'post_type'  => 'booking',
			'meta_query' => array(
				array(
					'key'     => 'villa',
					'value'   => $post_id,
					'compare' => 'LIKE'
				)
			)
		);

		$query = new WP_Query( $args );

		return $query;
	}

	/**
	 * print out the calendar
	 */
	public function generate_calendar( $bookings ) {

		$startdate = date( "Y-m-d", time() );

		$enddate = date( "Y-m-d", strtotime( "+12 months", time() ) );

		//Determine the period between now and twelve months from now as an array of objects
		$start    = ( new DateTime( $startdate ) )->modify( 'first day of this month' );
		$end      = ( new DateTime( $enddate ) )->modify( 'first day of this month' );
		$interval = DateInterval::createFromDateString( '1 month' );

		$period = new DatePeriod( $start, $interval, $end );

		$content = '<div class="cal">';

		//Foreach month, construct a calendar
		foreach ( $period as $dt ) {
			$content .= $this->generate_calendar_cell( $dt, $bookings );
			$this->currentDay = null;
		}

		$content .= <<<EOT
		</div>
EOT;

		return $content;
	}

	private function generate_calendar_cell( $period, $bookings ) {
		$month = $period->format( "F" );

		$this->daysInMonth = $period->format( 't' );

		$content = <<<EOT
		<div class="cal-container">
			<div class="cal__month">
			<h5>$month</h5>
			<table class="cal__table" >
EOT;

		foreach ( $this->dayLabels as $label ) {
			$content .= '<th>' . $label . '</th>';
		}

		for ( $i = 0; $i < 6; $i ++ ) {
			$content .= '<tr>';
			for ( $j = 1; $j <= 7; $j ++ ) {
				$content .= $this->_showDay( $i * 7 + $j, $period, $bookings );
			}
		}

		$content .= <<<EOT
				</table>
			</div>
		</div>
EOT;


		echo $content;
	}

	private function _showDay( $cellNumber, $period, $bookings ) {

		$posts       = $bookings->get_posts();
		$startClass  = null;
		$endClass    = null;
		$bookedClass = null;

		if ( $this->currentDay == 0 ) {

			$firstDayOfTheWeek = $period->format( 'N' );

			if ( intval( $cellNumber ) == intval( $firstDayOfTheWeek ) ) {

				$this->currentDay = 1;

			}
		}

		if ( ( $this->currentDay != 0 ) && ( $this->currentDay <= $this->daysInMonth ) ) {

			$this->currentDate = new DateTime( $period->format( 'Y-m' ) . '-' . $this->currentDay );

			//Check if current date is a start or end date of a booking
			foreach ( $posts as $post ) {
				$bookingStart = new DateTime( $post->start_date );
				$bookingEnd   = new DateTime( $post->end_date );

				if ( $this->currentDate->getTimestamp() == $bookingStart->getTimestamp() ) {
					$startClass = true;
				} elseif ( $this->currentDate->getTimestamp() == $bookingEnd->getTimestamp() ) {
					$endClass = true;
				} elseif ( $this->currentDate->getTimestamp() > $bookingStart->getTimestamp() && $this->currentDate->getTimestamp() < $bookingEnd->getTimestamp() ) {
					$bookedClass = true;
				}
			}

			$cellContent = $this->currentDay;

			$this->currentDay ++;

		} else {

			$this->currentDate = null;

			$cellContent = null;
		}


		if ( $startClass ) {
			$output = '<td class="booked booked--start">' . $cellContent . '</td>';
		} elseif ( $endClass ) {
			$output = '<td class="booked booked--end">' . $cellContent . '</td>';
		} elseif ( $bookedClass ) {
			$output = '<td class="booked">' . $cellContent . '</td>';
		} else {
			$output = '<td>' . $cellContent . '</td>';
		}

		return $output;
	}

	/**
	 * calculate number of weeks in a particular month
	 */
	private function _weeksInPeriod( $period ) {

		// find number of days in this month
		$daysInPeriod = $period->format( 't' );

		$numOfweeks = ( $daysInPeriod % 7 == 0 ? 0 : 1 ) + intval( $daysInPeriod / 7 );

		$monthEndingDay = date( 'N', strtotime( $period->format( 'Y' ) . ' - ' . $period->format( 'm' ) . ' - ' . $daysInPeriod ) );

		$monthStartDay = date( 'N', strtotime( $period->format( 'Y' ) . ' - ' . $period->format( 'm' ) . ' - 01' ) );

		if ( $monthEndingDay < $monthStartDay ) {

			$numOfweeks ++;

		}

		return $numOfweeks;
	}

	/**
	 * create calendar week labels
	 */
	private function _createLabels() {

		$content = '';

		foreach ( $this->dayLabels as $index => $label ) {

			$content .= ' < li class="' . ( $label == 6 ? 'end title' : 'start title' ) . ' title" > ' . $label . '</li > ';

		}

		return $content;
	}

}